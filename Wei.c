#include <fstream>
#include <TH2.h>
#include <TRandom.h>
#include <TLorentzVector.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <algorithm>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <THStack.h>
#include <iostream>
void Wei(const char* input1,const char* input2,const char* input3,TString intree, TString intreeb, TString outtree, const char*output, const char* Input){  
  //No.1,2,3 = sig, No.4,5,6 = bkg
  
  TFile *f1 = TFile::Open(input1);
  TFile *f2 = TFile::Open(input2);
  TFile *f3 = TFile::Open(input3);

  TFile *F = TFile::Open(Input);

  TFile file1(output,"update");

  TTree tree1(outtree,"photon");

  TTree *t1 = (TTree*) f1->Get(intree);
  TTree *t2 = (TTree*) f2->Get(intree);
  TTree *t3 = (TTree*) f3->Get(intree);
  TTree *t4 = (TTree*) f1->Get(intreeb);
  TTree *t5 = (TTree*) f2->Get(intreeb);
  TTree *t6 = (TTree*) f3->Get(intreeb);
  TTree *T = (TTree*) F->Get(intree);
   
  //Pt
  Int_t number = 31;
  
  const Double_t xEdges[32] = {10.0,12.0,14.0,16.0,18.0,20.0,22.0,24.0,26.0,28.0,30.0,
			32.0,34.0,36.0,38.0,40.0,42.0,44.0,46.0,48.0,50.0,55.0,
			60.0,65.0,70.0,75.0,80.0,90.0,100.0,150.0,200.0,250.0};
  /*
  const Double_t xEdges[33] = {10.0,17.5,25.0,32.5,40.0,47.5,55.0,62.5,70.0,77.5,85.0,
			       92.5,100.0,107.5,115.0,122.5,130.0,137.5,145.0,152.5,160.0,167.5,
			       175.0,182.5,190.0,197.5,205.0,212.5,220.0,227.5,235.0,242.5,250.0};
  */
   //Eta
  Int_t Number = 10.0;
  Double_t Ulimit = 0.;
  Double_t Dlimit = 0.;
 
  if(intree == "sEEvent" || intree == "bEEvent")
    {
      Ulimit = 2.5;
      Dlimit = 1.566;
    }
  else if(intree == "sBEvent" || intree == "bBEvent")
    {
      Ulimit = 1.4442;
      Dlimit = 0.0;
    }
  cout<<Ulimit<<"==="<<Dlimit<<"==="<<intree<<"==="<<intreeb<<endl;

  Float_t reco_Pt_1,reco_Pt_2,reco_Pt_3,reco_Pt_4,reco_Pt_5,reco_Pt_6,ScEta1,ScEta2,ScEta3,ScEta4,ScEta5,ScEta6;
  Float_t mcwei1,mcwei2,mcwei3,mcwei4,mcwei5,mcwei6;
  Float_t recoPt,phoSCRawE,phoR9,phoSigmaIEtaIEta,phoSCPhiWidth,phoSCEtaWidth,phoSigmaIEtaIPhi,phoS4,phoSCEta,phoESEffSigmaRR,rho,phoHoverE,ChoverPt,phoPFPhoIso,phoPFCHIso,phoPFCHWorstIso,phoESoverE,phoDeltaR,diPhoM,rewei,mcwei,weight,phoIDMVA;
  
  Float_t reco_Pt,reco_Eta,reco_Phi,Sceta,r9,rawE,sigmaIEtaIEta,Phiwidth,Etawidth,sigmaIEtaIPhi,s4,sigmaRR,Rho,phoIso,chIso,
    chworstIso,esEoverrawE,HoverE,diphoM,DeltaR,weights,IDMVA;
 
  vector<float> rewei1;
  vector<float> rewei2;
  vector<float> rewei3;
  vector<float> rewei4;
  vector<float> rewei5;
  vector<float> rewei6;
  vector<float> reweia;
  vector<float> reweib;
  vector<float> reweic;
  vector<float> reweid;
  vector<float> reweiw;

  vector<vector<float> > reweiW;
   
  tree1.Branch("recoPt",&recoPt,"recoPt/F");
  tree1.Branch("phoSCRawE",&phoSCRawE,"phoSCRawE/F");
  tree1.Branch("phoR9",&phoR9,"phoR9/F");
  tree1.Branch("phoSigmaIEtaIEta",&phoSigmaIEtaIEta,"phoSigmaIEtaIEta/F");
  tree1.Branch("phoSCPhiWidth",&phoSCPhiWidth,"phoSCPhiWidth/F");
  tree1.Branch("phoSCEtaWidth",&phoSCEtaWidth,"phoSCEtaWidth/F");
  tree1.Branch("phoSigmaIEtaIPhi",&phoSigmaIEtaIPhi,"phoSigmaIEtaIPhi/F");
  tree1.Branch("phoS4",&phoS4,"phoS4/F");
  tree1.Branch("phoSCEta",&phoSCEta,"phoSCEta/F");
  tree1.Branch("phoESEffSigmaRR",&phoESEffSigmaRR,"phoESEffSigmaRR/F");
  tree1.Branch("rho",&rho,"rho/F");
  tree1.Branch("phoPFPhoIso",&phoPFPhoIso,"phoPFPhoIso/F");
  tree1.Branch("phoPFCHIso",&phoPFCHIso,"phoPFCHIso/F");
  tree1.Branch("phoPFCHWorstIso",&phoPFCHWorstIso,"phoPFCHWorstIso/F");
  tree1.Branch("phoESoverE",&phoESoverE,"phoESoverE/F");
  tree1.Branch("phoDeltaR",&phoDeltaR,"phoDeltaR/F");
  tree1.Branch("phoHoverE",&phoHoverE,"phoHoverE/F");
  tree1.Branch("ChoverPt",&ChoverPt,"ChoverPt/F");
  tree1.Branch("rewei",&rewei,"rewei/F");
  tree1.Branch("mcwei",&mcwei,"mcwei/F");
  tree1.Branch("phoIDMVA",&phoIDMVA,"phoIDMVA/F");
  //PtEta
  
  TH2F *h1 = new TH2F("h1","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *h2 = new TH2F("h2","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *h3 = new TH2F("h3","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *h4 = new TH2F("h4","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *h5 = new TH2F("h5","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *h6 = new TH2F("h6","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *hw = new TH2F("hw","",number,xEdges,Number,Dlimit,Ulimit);
  TH2F *hW = new TH2F("hW","",number,xEdges,Number,Dlimit,Ulimit);
     
  T->SetBranchAddress("reco_Pt",&reco_Pt);
  T->SetBranchAddress("reco_Eta",&reco_Eta);
  T->SetBranchAddress("reco_Phi",&reco_Phi);
  T->SetBranchAddress("Sceta",&Sceta);
  T->SetBranchAddress("r9",&r9);
  T->SetBranchAddress("rawE",&rawE);
  T->SetBranchAddress("sigmaIEtaIEta",&sigmaIEtaIEta);
  T->SetBranchAddress("Phiwidth",&Phiwidth);
  T->SetBranchAddress("Etawidth",&Etawidth);
  T->SetBranchAddress("sigmaIEtaIPhi",&sigmaIEtaIPhi);
  T->SetBranchAddress("s4",&s4);
  T->SetBranchAddress("sigmaRR",&sigmaRR);
  T->SetBranchAddress("Rho",&Rho);
  T->SetBranchAddress("phoIso",&phoIso);
  T->SetBranchAddress("chIso",&chIso);
  T->SetBranchAddress("chworstIso",&chworstIso);
  T->SetBranchAddress("esEoverrawE",&esEoverrawE);
  T->SetBranchAddress("HoverE",&HoverE);
  T->SetBranchAddress("weights",&weights);
  T->SetBranchAddress("IDMVA",&IDMVA);
  //PtEta
  for(Long64_t ev1 = 0; ev1<t1->GetEntriesFast(); ev1++)
    {
      t1->GetEntry(ev1);
      t1->SetBranchAddress("reco_Pt", &reco_Pt_1);
      t1->SetBranchAddress("Sceta", &ScEta1);
      t1->SetBranchAddress("weights",&mcwei1);
      h1->Fill(reco_Pt_1,fabs(ScEta1),mcwei1);
    }
  for(Long64_t ev2 = 0; ev2<t2->GetEntriesFast(); ev2++)
    {
      t2->GetEntry(ev2);
      t2->SetBranchAddress("reco_Pt", &reco_Pt_2);
      t2->SetBranchAddress("Sceta", &ScEta2);
      t2->SetBranchAddress("weights",&mcwei2);
      h2->Fill(reco_Pt_2,fabs(ScEta2),mcwei2);
    }
  for(Long64_t ev3 = 0; ev3<t3->GetEntriesFast(); ev3++)
    {
      t3->GetEntry(ev3);
      t3->SetBranchAddress("reco_Pt", &reco_Pt_3);
      t3->SetBranchAddress("Sceta", &ScEta3);
      t3->SetBranchAddress("weights",&mcwei3);
      h3->Fill(reco_Pt_3,fabs(ScEta3),mcwei3);
    }
  for(Long64_t ev4 = 0; ev4<t4->GetEntriesFast(); ev4++)
    {
      t4->GetEntry(ev4);
      t4->SetBranchAddress("reco_Pt", &reco_Pt_4);
      t4->SetBranchAddress("Sceta", &ScEta4);
      t4->SetBranchAddress("weights",&mcwei4);
      h4->Fill(reco_Pt_4,fabs(ScEta4),mcwei4);
    }
  for(Long64_t ev5 = 0; ev5<t5->GetEntriesFast(); ev5++)
    {
      t5->GetEntry(ev5);
      t5->SetBranchAddress("reco_Pt", &reco_Pt_5);
      t5->SetBranchAddress("Sceta", &ScEta5);
      t5->SetBranchAddress("weights",&mcwei5);
      h5->Fill(reco_Pt_5,fabs(ScEta5),mcwei5);
    }
  for(Long64_t ev6 = 0; ev6<t6->GetEntriesFast(); ev6++)
    {
      t6->GetEntry(ev6);
      t6->SetBranchAddress("reco_Pt", &reco_Pt_6);
      t6->SetBranchAddress("Sceta", &ScEta6);
      t6->SetBranchAddress("weights",&mcwei6);
      h6->Fill(reco_Pt_6,fabs(ScEta6),mcwei6);
    }
  cout<<"debug182"<<endl;
  /////////////////////////////////////////////////////////////////////////////////////////
  //i = PtEta, j = Eta
  
  for(int i = 0; i<number; i++)
    {
    rewei1.clear();
    rewei2.clear();
    rewei3.clear();
    rewei4.clear();
    rewei5.clear();
    rewei6.clear();
    reweia.clear();
    reweib.clear();
    for(int j = 0; j<Number; j++)
      {
	rewei1.push_back(h1->GetBinContent(i+1,j+1));
	rewei2.push_back(h2->GetBinContent(i+1,j+1));
	rewei3.push_back(h3->GetBinContent(i+1,j+1));
	reweia.push_back(rewei1[j]+rewei2[j]+rewei3[j]);
	hw->SetBinContent(i+1,j+1,reweia[j]);

	rewei4.push_back(h4->GetBinContent(i+1,j+1));
	rewei5.push_back(h5->GetBinContent(i+1,j+1));
	rewei6.push_back(h6->GetBinContent(i+1,j+1));
	reweib.push_back(rewei4[j]+rewei5[j]+rewei6[j]);
	// cout<<reweia[j]<<"==="<<reweib[j]<<endl;
	hW->SetBinContent(i+1,j+1,reweib[j]);
      }
  }
  hw->Scale(1./hw->Integral(-1,-1,-1,-1));
  hW->Scale(1./hW->Integral(-1,-1,-1,-1));
  //Wei = sig, WEI = bkg 
  TH2F *hWeight = (TH2F*)hW->Clone("hWeight");
  hWeight->Divide(hw); 
  
  Long64_t N = T->GetEntriesFast();
 
  for(Long64_t a = 0; a<N; a++)
    {
      if (a % 10000 == 0)
	fprintf(stderr, "Processing event %lli of %lli\n", a + 1, N);
      T->GetEntry(a);
      recoPt = reco_Pt;
      phoR9 = r9;
      phoSigmaIEtaIEta = sigmaIEtaIEta;
      phoSCEtaWidth = Etawidth;
      phoSCPhiWidth = Phiwidth;
      phoSigmaIEtaIPhi = sigmaIEtaIPhi;
      phoS4 = s4;
      phoSCEta = Sceta;
      phoESEffSigmaRR = sigmaRR;
      rho = Rho;
      phoPFPhoIso = phoIso;
      phoPFCHIso = chIso;
      phoPFCHWorstIso = chworstIso;
      phoESoverE = esEoverrawE;
      phoSCRawE = rawE;
      phoHoverE = HoverE;
      ChoverPt = chIso/reco_Pt;
      mcwei = weights;
      phoIDMVA = IDMVA;
     
      // cout<<"debug265"<<endl;
      int i = hWeight->GetXaxis()->FindBin(recoPt);
      int j = hWeight->GetYaxis()->FindBin(fabs(phoSCEta));
      rewei = hWeight->GetBinContent(i,j)*mcwei;
      tree1.Fill();
    }
  tree1.Write();
  /*  
  TCanvas *C = new TCanvas("C","C",600,600);
  C->cd();
  h1->Draw("colz");
  */
}
