#include <iostream>
#include <map>
#include <string>

#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TObjString.h"
#include "TSystem.h"
#include "TROOT.h"

#include "TMVA/Factory.h"
#include "TMVA/Tools.h"
#include "TMVA/DataLoader.h"
//#include "TMVA/TMVAGui.h"
//#include "Untuplizer_07.h" 

void HZg_MVA(const char* input_sig1, const char* input_sig2, const char* input_sig3, const char* input_bkg1, const char* input_bkg2, const char* input_bkg3,TString myMethodList,TString rootfile,TString filename,TString classfication){

  
  // double mcwei[6] = {1,1,1,1,1,1};
  TString sampleNames = rootfile;
  TString outFileName = "/home/jkl596612/work/HZg/MVA_5e05/TMVAroot/16" + filename +"_"+sampleNames + "_"+"TMVA.root";
  TString ClassficationBaseName = classfication;

  TMVA::Tools::Instance();

  std::map<std::string, int> Use;

  if (myMethodList != "")
    {
      for (std::map<std::string, int>::iterator it = Use.begin(); it != Use.end(); it++) it->second = 0;

      std::vector<TString> mlist = TMVA::gTools().SplitString(myMethodList, ',');
      for (UInt_t i = 0; i < mlist.size(); i++) {
	std::string regMethod(mlist[i]);

	if (Use.find(regMethod) == Use.end()) {
	  std::cout << "Method \"" << regMethod << "\" not known in TMVA under this name. Choose among the following:" << std::endl;
	  for (std::map<std::string, int>::iterator it = Use.begin(); it != Use.end(); it++) std::cout << it->first << " ";
	  std::cout << std::endl;
	  return;
	}
	Use[regMethod] = 1;
      }
    }
  
  TFile* outputFile = TFile::Open(outFileName, "RECREATE");
  TMVA::Factory *factory = new TMVA::Factory(ClassficationBaseName, outputFile,
					      "!V:!Silent:Color:DrawProgressBar:Transformations=I;D;G,D:AnalysisType=Classification");

   TMVA::DataLoader *dataloader = new TMVA::DataLoader("dataset");
   
   TFile *gj_20to40 = TFile::Open(input_sig1);
   TFile *gj_20 = TFile::Open(input_sig2);
   TFile *gj_40 = TFile::Open(input_sig3);
   TFile *bg_20to40 = TFile::Open(input_bkg1);
   TFile *bg_20 = TFile::Open(input_bkg2);
   TFile *bg_40 = TFile::Open(input_bkg3);

   cout<<"input signal file"<<endl;

   TTree *t_20to40 = (TTree*)gj_20to40->Get(rootfile);
   TTree *t_20 = (TTree*)gj_20->Get(rootfile);
   TTree *t_40 = (TTree*)gj_40->Get(rootfile);
   TTree *b_20to40 = (TTree*)bg_20to40->Get(rootfile);
   TTree *b_20 = (TTree*)bg_20->Get(rootfile);
   TTree *b_40 = (TTree*)bg_40->Get(rootfile);


   if(rootfile == "EEevent")
     {
       dataloader->AddVariable("phoSCRawE","phoSCRawE","",'F');//No.0
       dataloader->AddVariable("phoS4","phoS4","",'F');
       dataloader->AddVariable("phoR9","phoR9","",'F');
       dataloader->AddVariable("phoSCEta","Sc#eta","",'F');    
       dataloader->AddVariable("phoSCPhiWidth","#phiwidth","",'F');
       dataloader->AddVariable("phoSCEtaWidth","#etawidth","",'F');
       dataloader->AddVariable("phoSigmaIEtaIPhi","#sigmaI#etaI#phi","",'F');      
       dataloader->AddVariable("phoSigmaIEtaIEta","#sigmaI#etaI#eta","",'F');//No.7
       dataloader->AddVariable("rho","Rho","",'F');
       dataloader->AddVariable("phoPFPhoIso","phoPFPhoIso","",'F');
       dataloader->AddVariable("phoPFCHIso","phoPFCHIso","",'F');
       dataloader->AddVariable("phoPFCHWorstIso","phoPFCHWorstIso","",'F');
       dataloader->AddVariable("phoESoverE","phoESoverE","",'F');
       dataloader->AddVariable("phoESEffSigmaRR","#phoESEffSigmaRR","",'F');
     }
   if(rootfile == "EBevent")
     {
       dataloader->AddVariable("phoSCRawE","phoSCRawE","",'F');//No.0
       dataloader->AddVariable("phoS4","phoS4","",'F'); 
       dataloader->AddVariable("phoR9","phoR9","",'F');
       dataloader->AddVariable("phoSCEta","Sc#eta","",'F');
       dataloader->AddVariable("phoSCPhiWidth","#phiwidth","",'F');
       dataloader->AddVariable("phoSCEtaWidth","#etawidth","",'F');
       dataloader->AddVariable("phoSigmaIEtaIPhi","#sigmaI#etaI#phi","",'F');//No.7
       dataloader->AddVariable("phoSigmaIEtaIEta","#sigmaI#etaI#eta","",'F');    
       dataloader->AddVariable("rho","Rho","",'F');
       dataloader->AddVariable("phoPFPhoIso","phoPFPhoIso","",'F');
       dataloader->AddVariable("phoPFCHIso","phoPFCHIso","",'F');
       dataloader->AddVariable("phoPFCHWorstIso","phoPFCHWorstIso","",'F');
     }

   cout<<"debug"<<endl;
   dataloader->AddSignalTree(t_20to40);
   dataloader->AddSignalTree(t_20);
   dataloader->AddSignalTree(t_40);
   dataloader->AddBackgroundTree(b_20to40);
   dataloader->AddBackgroundTree(b_20);
   dataloader->AddBackgroundTree(b_40);

   cout<<"debug127"<<endl;
    
   dataloader->SetSignalWeightExpression("rewei");
   dataloader->SetBackgroundWeightExpression("rewei");

   cout<<"debug131"<<endl;
   TCut mycuts;
   TCut mycutb;
 
   if(rootfile == "EEevent")
     {
       mycuts = "phoSigmaIEtaIPhi < 1 && phoS4 > -2 && phoS4 < 2";
       mycutb = "phoSigmaIEtaIPhi < 1 && phoS4 > -2 && phoS4 < 2";
     }
   else if(rootfile == "EBevent")
     {
       mycuts = "phoSigmaIEtaIPhi <= 1 && phoS4 > -2 && phoS4 < 2";
       mycutb = "phoSigmaIEtaIPhi <= 1 && phoS4 > -2 && phoS4 < 2";
     }
  
      
    dataloader->PrepareTrainingAndTestTree( mycuts, mycutb,
					   "nTrain_Signal=0:nTrain_Background=0:SplitMode=Random:NormMode=EqualNumEvents:!V" ); //NumEvents       
    factory->BookMethod(dataloader,TMVA::Types::kBDT, "BDTG", "!H:!V:NTrees=2000:BoostType=Grad:Shrinkage=0.10:!UseBaggedGrad:nCuts=2000:UseNvars=4:PruneStrength=5:PruneMethod=CostComplexity:MaxDepth=6");

   factory->TrainAllMethods();
   factory->TestAllMethods();
   cout<<"?"<<endl;
   factory->EvaluateAllMethods();
   
   outputFile->Close();
   delete factory;
   delete dataloader;
   //  if (!gROOT->IsBatch()) TMVA::TMVAGui( outFileName );
}
