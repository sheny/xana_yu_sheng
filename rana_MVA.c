{
  gSystem->SetBuildDir("tmpdir", kTRUE);

  gROOT->ProcessLine(".L MVA_HZg.c++");

  char input_sig_path_wei_20to40[500];
  sprintf(input_sig_path_wei_20to40,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_sig_gjet_pt20to40_MGG_80toInf.root");
  char input_sig_path_wei_20[500];
  sprintf(input_sig_path_wei_20,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_sig_gjet_pt20_MGG_40to80.root");
  char input_sig_path_wei_40[500];
  sprintf(input_sig_path_wei_40,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_sig_gjet_pt40_MGG_80toInf.root");
  char input_bkg_path_wei_20to40[500];
  sprintf(input_bkg_path_wei_20to40,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_bkg_gjet_pt20to40_MGG_80toInf.root");
  char input_bkg_path_wei_20[500];
  sprintf(input_bkg_path_wei_20,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_bkg_gjet_pt20_MGG_40to80.root");
  char input_bkg_path_wei_40[500];
  sprintf(input_bkg_path_wei_40,"/home/jkl596612/work/HZg/MVA_5e05/16/rew_test_train/pho_bkg_gjet_pt40_MGG_80toInf.root");

  TString method = "";
  
  TString rootfile;
  TString rootname;
  TString filename;
  TString preselection;
  /*

  */
      filename = "gjet_test_wei";
      for(int i = 0; i<2; i++)
	{
	  if(i == 1)
                {
                  rootfile = "EEevent";
		  rootname = "gjet_test_EE_wei";
                  HZg_MVA(input_sig_path_wei_20to40,input_sig_path_wei_20,input_sig_path_wei_40,input_bkg_path_wei_20to40,input_bkg_path_wei_20,input_bkg_path_wei_40,method,rootfile,filename,rootname);
                }
              else if(i == 0)
		{
                  rootfile = "EBevent";
		  rootname = "gjet_test_EB_wei";
                  HZg_MVA(input_sig_path_wei_20to40,input_sig_path_wei_20,input_sig_path_wei_40,input_bkg_path_wei_20to40,input_bkg_path_wei_20,input_bkg_path_wei_40,method,rootfile,filename,rootname);
                }
	    }
    
}

