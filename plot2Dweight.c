
#include <fstream>
#include <TH1F.h>
#include <TRandom.h>
#include <TLorentzVector.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TStyle.h>
#include <algorithm>
#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <THStack.h>
#include <iostream>
#include <TH1F.h>
void plot2Dweight(){

  gStyle->SetOptStat(1111111);

  TFile *f1 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_sig_gjet_pt20to40_MGG_80toInf.root","update");
  TFile *f2 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_sig_gjet_pt20_MGG_40to80.root","update");
  TFile *f3 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_sig_gjet_pt40_MGG_80toInf.root","update");
  TFile *f4 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_bkg_gjet_pt20to40_MGG_80toInf.root","update");
  TFile *f5 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_bkg_gjet_pt20_MGG_40to80.root","update");
  TFile *f6 = new TFile("/home/jkl596612/work/HZg/MVA_5e05/wei_gjetf/pho_bkg_gjet_pt40_MGG_80toInf.root","update");

  TTree *t1 = (TTree*) f1->Get("EBevent");
  TTree *t2 = (TTree*) f2->Get("EBevent");
  TTree *t3 = (TTree*) f3->Get("EBevent");
  TTree *t4 = (TTree*) f4->Get("EBevent");
  TTree *t5 = (TTree*) f5->Get("EBevent");
  TTree *t6 = (TTree*) f6->Get("EBevent");

  Int_t number = 60;
  // Double_t xEdges[32] = {10.0,12.0,14.0,16.0,18.0,20.0,22.0,24.0,26.0,28.0,30.0,
  //			 32.0,34.0,36.0,38.0,40.0,42.0,44.0,46.0,48.0,50.0,55.0,
  //			 60.0,65.0,70.0,75.0,80.0,90.0,100.0,150.0,200.0,250.0};
  
  Double_t MCwei[6] = {35.9*1000*137751/10000000,
                       35.9*1000*154500/10000000,
                       35.9*1000*16792/10000000,
                       137751*35.9*1000/10000000,
                       154500*35.9*1000/10000000,
                       35.9*1000*16792/10000000};
  
  Float_t reco_Pt_1,reco_Pt_2,reco_Pt_3,reco_Pt_4,reco_Pt_5,reco_Pt_6,
    ScEta1,ScEta2,ScEta3,ScEta4,ScEta5,ScEta6,rewei_1,rewei_2,rewei_3;
  Float_t recoPt,phoSCEta,rewei;

  vector<double> rewei1;
  vector<double> rewei2;
  vector<double> rewei3;
  vector<double> rewei4;
  vector<double> rewei5;
  vector<double> rewei6;
  vector<double> rewei7;
  vector<double> rewei8;
  vector<double> rewei9;
  vector<double> reweia;
  vector<double> reweib;
  vector<double> reweic;

  TH1F *h1 = new TH1F("h1","",number,10.0,250.0);
  TH1F *h2 = new TH1F("h2","",number,10.0,250.0);
  TH1F *h3 = new TH1F("h3","",number,10.0,250.0);
  TH1F *h4 = new TH1F("h4","",number,10.0,250.0);
  TH1F *h5 = new TH1F("h5","",number,10.0,250.0);
  TH1F *h6 = new TH1F("h6","",number,10.0,250.0);
  TH1F *h7 = new TH1F("h7","",number,10.0,250.0);
  TH1F *h8 = new TH1F("h8","",number,10.0,250.0);
  TH1F *h9 = new TH1F("h9","",number,10.0,250.0);

  for(Long64_t ev1 = 0; ev1<t1->GetEntriesFast(); ev1++)
    {
      t1->GetEntry(ev1);
      t1->SetBranchAddress("recoPt", &reco_Pt_1);
      t1->SetBranchAddress("phoSCEta", &ScEta1);
      t1->SetBranchAddress("rewei", &rewei_1);
      h1->Fill(reco_Pt_1,MCwei[0]);
      h7->Fill(reco_Pt_1,rewei_1*MCwei[0]);
    }
  for(Long64_t ev2 = 0; ev2<t2->GetEntriesFast(); ev2++)
    {
      t2->GetEntry(ev2);
      t2->SetBranchAddress("recoPt", &reco_Pt_2);
      t2->SetBranchAddress("phoSCEta", &ScEta2);
      t2->SetBranchAddress("rewei", &rewei_2);
      h2->Fill(reco_Pt_2,MCwei[1]);
      h8->Fill(reco_Pt_2,rewei_2*MCwei[1]);
    }
  for(Long64_t ev3 = 0; ev3<t3->GetEntriesFast(); ev3++)
    {
      t3->GetEntry(ev3);
      t3->SetBranchAddress("recoPt", &reco_Pt_3);
      t3->SetBranchAddress("phoSCEta", &ScEta3);
      t3->SetBranchAddress("rewei", &rewei_3);
      h3->Fill(reco_Pt_3,MCwei[2]);
      h9->Fill(reco_Pt_3,rewei_3*MCwei[2]);
    }
  for(Long64_t ev4 = 0; ev4<t4->GetEntriesFast(); ev4++)
    {
      t4->GetEntry(ev4);
      t4->SetBranchAddress("recoPt", &reco_Pt_4);
      t4->SetBranchAddress("phoSCEta", &ScEta4);
      h4->Fill(reco_Pt_4,MCwei[3]);
    }
  for(Long64_t ev5 = 0; ev5<t5->GetEntriesFast(); ev5++)
    {
      t5->GetEntry(ev5);
      t5->SetBranchAddress("recoPt", &reco_Pt_5);
      t5->SetBranchAddress("phoSCEta", &ScEta5);
      h5->Fill(reco_Pt_5,MCwei[4]);
    }
  for(Long64_t ev6 = 0; ev6<t6->GetEntriesFast(); ev6++)
    {
      t6->GetEntry(ev6);
      t6->SetBranchAddress("recoPt", &reco_Pt_6);
      t6->SetBranchAddress("phoSCEta", &ScEta6);
      h6->Fill(reco_Pt_6,MCwei[5]);
    }

  TH1D *hPt_Sig = new TH1D("hPt_Sig","Pt distribution",number,10.0,250.0);
  TH1D *hPt_Bkg = new TH1D("hPt_Bkg","Pt distribution",number,10.0,250.0);
  TH1D *hPt_rew = new TH1D("hPt_rew","Pt distribution",number,10.0,250.0);
  THStack *hSig = new THStack("hSig"," ;BDT score");
  hSig->Add(h1);
  hSig->Add(h2);
  hSig->Add(h3);

  h1->SetLineColor(kRed);
  h1->SetMarkerColor(kRed);
  h1->SetMarkerStyle(21);
  h1->SetMarkerSize(0.6);

  h2->SetLineColor(kBlue);
  h2->SetMarkerColor(kBlue);
  h2->SetMarkerStyle(21);
  h2->SetMarkerSize(0.6);

  h3->SetLineColor(kGreen+1);
  h3->SetMarkerColor(kGreen+1);
  h3->SetMarkerStyle(21);
  h3->SetMarkerSize(0.6);


  hPt_Sig->SetLineColor(kRed);
  hPt_Sig->SetMarkerColor(kRed);
  hPt_Sig->SetMarkerStyle(21);
  hPt_Sig->SetMarkerSize(0.6);

  hPt_Bkg->SetLineColor(kBlue);
  hPt_Bkg->SetMarkerColor(kBlue);
  hPt_Bkg->SetMarkerStyle(21);
  hPt_Bkg->SetMarkerSize(0.6);

  hPt_rew->SetLineColor(kGreen+1);
  hPt_rew->SetMarkerColor(kGreen+1);
  hPt_rew->SetMarkerStyle(21);
  hPt_rew->SetMarkerSize(0.8);

  for(float i = 0; i<number; i++)
    {
      rewei1.push_back(h1->GetBinContent(i+1));
      rewei2.push_back(h2->GetBinContent(i+1));
      rewei3.push_back(h3->GetBinContent(i+1));
      reweia.push_back(rewei1[i]+rewei2[i]+rewei3[i]);
      hPt_Sig->SetBinContent(i+1,reweia[i]);

      rewei4.push_back(h4->GetBinContent(i+1));
      rewei5.push_back(h5->GetBinContent(i+1));
      rewei6.push_back(h6->GetBinContent(i+1));
      reweib.push_back(rewei4[i]+rewei5[i]+rewei6[i]);
      // cout<<reweia[i]<<"==="<<reweib[i]<<endl;                                                                          
      hPt_Bkg->SetBinContent(i+1,reweib[i]);

      rewei7.push_back(h7->GetBinContent(i+1));
      rewei8.push_back(h8->GetBinContent(i+1));
      rewei9.push_back(h9->GetBinContent(i+1));
      reweic.push_back(rewei7[i]+rewei8[i]+rewei9[i]);
      hPt_rew->SetBinContent(i+1,reweic[i]);
    }

  hPt_Bkg->Scale(1./hPt_Bkg->Integral(-1,-1));
  hPt_Sig->Scale(1./hPt_Sig->Integral(-1,-1));
  hPt_rew->Scale(1./hPt_rew->Integral(-1,-1));
 
 
  TCanvas *c = new TCanvas("c","c",800,600);
  c->cd();
  hPt_Bkg->SetStats(0);
  hPt_Bkg->SetMinimum(0.0);
  //hBkg->GetHistogram()->GetYaxis()->SetTitleOffset(0.);
  hPt_Bkg->Draw();
  hPt_Sig->Draw("same");
  hPt_rew->Draw("same");
  hPt_Bkg->GetXaxis()->SetTitle("P_{T} GeV");
  hPt_Bkg->GetXaxis()->SetRange(0.0,250.0);
  hPt_Bkg->GetYaxis()->SetTitle("Events");
  hPt_Bkg->GetYaxis()->SetTitleOffset(1.5);
  //  TLatex latex;
  //  latex.SetTextSize(0.05);
  //  latex.DrawLatex(0.4,0.053,Form("35.9 fb^{-1} 13TeV",1));
  //  latex.DrawLatex(-1.0,0.053,Form("CMS  work-in-process",1));
  TLegend*legend = new TLegend(0.75,0.75,0.9,0.9);
  legend->SetTextSize(0.03);
  legend->AddEntry(hPt_Bkg,"Pt_Bkg","pl");
  legend->AddEntry(hPt_Sig,"Pt_Sig","pl");
  legend->AddEntry(hPt_rew,"Pt_reweight","pl");
  legend->SetFillColor(0);
  legend->Draw();
  /*
  TCanvas *c1 = new TCanvas("c1","c1",800,600);
  c1->cd();
  //  hSig->SetStats(0);
  hSig->Draw();
  TLegend*legend1 = new TLegend(0.75,0.75,0.9,0.9);
  legend1->SetTextSize(0.03);
  legend1->AddEntry(h1,"20to40","pl");
  legend1->AddEntry(h2,"20","pl");
  legend1->AddEntry(h3,"40","pl");
  legend1->SetFillColor(0);
  legend1->Draw();
  */

  c->Print("gjet_Pt_EB.pdf");
 

}


/*  LocalWords:  GetEntry
 */
