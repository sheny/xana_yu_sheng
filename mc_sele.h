#include <vector>
#include <iostream>
#include <TH1D.h>
#include <TRandom.h>
#include "untuplizer_07.h"
using namespace std;
void select_pho(TreeReader &data,vector<int> &accepted){
  // cout<<"debug3"<<endl;
  accepted.clear();
  Int_t   nMC  = data.GetInt("nMC");
  Int_t* mcPID = data.GetPtrInt("mcPID");
  Int_t* mcMomPID = data.GetPtrInt("mcMomPID");
  UShort_t* mcStatusFlag = NULL;
  mcStatusFlag= (UShort_t*) data.GetPtrShort("mcStatusFlag");
  Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
  Float_t* mcCalIsoDR04 = data.GetPtrFloat("mcCalIsoDR04");
   for (int i = 0; i < nMC; ++i) {
    if(mcPID[i] != 22) continue;
    // if(mcCalIsoDR04[i] > 6.)continue;
    // if(mcMomPID[i] != 25) continue;
    if(((mcStatusFlag[i] >> 0) & 1) == 0 || ((mcStatusFlag[i] >> 1) & 1) == 0)continue;
   
    accepted.push_back(i);
  }  
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void select_pho_HZg(TreeReader &data,vector<int> &accepted){
  // cout<<"debug3"<<endl;                                                                                                      
  accepted.clear();
  Int_t   nMC  = data.GetInt("nMC");
  Int_t* mcPID = data.GetPtrInt("mcPID");
  Int_t* mcMomPID = data.GetPtrInt("mcMomPID");
  UShort_t* mcStatusFlag = NULL;
  mcStatusFlag= (UShort_t*) data.GetPtrShort("mcStatusFlag");
  Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
  Float_t* mcCalIsoDR04 = data.GetPtrFloat("mcCalIsoDR04");
  for (int i = 0; i < nMC; ++i) {
    if(mcPID[i] != 22) continue;
    // if(mcCalIsoDR04[i] > 5.)continue;                                                                                        
    if(mcMomPID[i] != 25) continue;                                                                                            
    if(((mcStatusFlag[i] >> 0) & 1) == 0 || ((mcStatusFlag[i] >> 1) & 1) == 0)continue;

    accepted.push_back(i);
  }
}

