#include <vector>
#include <iostream>
#include <TH1D.h>
#include <TRandom.h>
#include "untuplizer_07.h"
using namespace std;
void pre_sele(TreeReader &data,vector<int> &accepted){
  accepted.clear();
  Float_t* phoR9Full5x5 = data.GetPtrFloat("phoR9Full5x5");
  Float_t* phoR9 = data.GetPtrFloat("phoR9");
  Float_t* phoSigmaIEtaIEtaFull5x5 = data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
  Float_t* phoEt   = data.GetPtrFloat("phoEt");
  Float_t* phoPFPhoIso = data.GetPtrFloat("phoPFPhoIso");
  Float_t* phoPFChIso = data.GetPtrFloat("phoPFChIso");
  Float_t* phoHoverE = data.GetPtrFloat("phoHoverE");
  Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
  Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
  Int_t  nPho  = data.GetInt("nPho");
  for (int i = 0; i < nPho; i++) {

    if(fabs(phoSCEta[i]) < 1.4442)
      {
	if(phoR9[i] > 0.85)
	  {
	    if(phoHoverE[i] > 0.08)continue;
	    if(phoEt[i] < 15)continue;
	    if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
	    if(phoR9Full5x5[i] <0.5)continue;
	  }
	else if(phoR9[i] < 0.85 || phoR9[i] == 0.85)
	  {
	    if(phoSigmaIEtaIEtaFull5x5[i] > 0.015)continue;
	    if(phoR9Full5x5[i] <0.5)continue;
            if(phoPFPhoIso[i] > 4)continue;
	    if(phoHoverE[i] > 0.08)continue;
            if(phoEt[i] < 15)continue;
            if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
	  }
      }
    else if(fabs(phoSCEta[i]) > 1.566 && fabs(phoSCEta[i]) < 2.5)
      {	    
	if(phoR9[i] > 0.9)
          {
            if(phoHoverE[i] > 0.08)continue;
            if(phoEt[i] < 15)continue;
            if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
            if(phoR9Full5x5[i] <0.8)continue;
          }
        else if(phoR9[i] < 0.9 || phoR9[i] == 0.9)
          {
            if(phoSigmaIEtaIEtaFull5x5[i] > 0.035)continue;
            if(phoR9Full5x5[i] <0.8)continue;
            if(phoPFPhoIso[i] > 4)continue;
            if(phoHoverE[i] > 0.08)continue;
            if(phoEt[i] < 15)continue;
            if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
          }
      }
    else continue;
    
    
    accepted.push_back(i);
  }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void pre_sele_samp(TreeReader &data,vector<int> &accepted1,vector<int> &accepted2,vector<int> &accepted3){
  accepted1.clear();
  accepted2.clear();
  accepted3.clear();
  Float_t* phoEt   = data.GetPtrFloat("phoEt");
  Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
  Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
  Float_t* eleSCEta = data.GetPtrFloat("eleSCEta");
  Float_t* muEta = data.GetPtrFloat("muEta");
  Int_t  nPho  = data.GetInt("nPho");
  Int_t  nEle  = data.GetInt("nEle");
  Int_t  nMu  = data.GetInt("nMu");
  for (int i = 0; i < nPho; i++) {
    if(fabs(phoSCEta[i]) < 1.4442)
      {
	if(phoEt[i] < 15)continue;
        if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
      }
    else if(fabs(phoSCEta[i]) > 1.566 && fabs(phoSCEta[i]) < 2.5)
      {
	if(phoEt[i] < 15)continue;
        if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
      }
    else continue;
    accepted1.push_back(i);
  }
  for (int j = 0; j < nEle; j++) {
    if(fabs(eleSCEta[j]) < 1.566 && fabs(eleSCEta[j]) > 1.4442)continue;
    else if(fabs(eleSCEta[j]) > 2.5)continue;
    else accepted2.push_back(j);
  }
  for (int k = 0; k < nMu; k++) {
    if(fabs(muEta[k]) < 1.566 && fabs(muEta[k]) > 1.4442)continue;
    else if(fabs(muEta[k]) > 2.5)continue;
    else accepted3.push_back(k);
  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void pre_sele_HZg(TreeReader &data,vector<int> &accepted1){
  accepted1.clear();
  Float_t* phoEt   = data.GetPtrFloat("phoEt");
  Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
  Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
  Int_t  nPho  = data.GetInt("nPho");
  for (int i = 0; i < nPho; i++) {
    if(fabs(phoSCEta[i]) < 1.4442)
      {
        if(phoEt[i] < 15)continue;
        if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
      }
    else if(fabs(phoSCEta[i]) > 1.566 && fabs(phoSCEta[i]) < 2.5)
      {
        if(phoEt[i] < 15)continue;
        if(phoSigmaIEtaIPhiFull5x5[i] > 1)continue;
      }
    else continue;
    accepted1.push_back(i);
  }
}
