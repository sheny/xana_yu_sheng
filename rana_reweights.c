{
  gSystem->SetBuildDir("tmpdir", kTRUE);

  gROOT->ProcessLine(".L Wei.c++");

  char input_path_wei_20to40[500];
  sprintf(input_path_wei_20to40,"/home/jkl596612/work/HZg/rewei/17/pho_gjet/mini_pho_gjet_test_pt20to40_MGG_80toInf.root");
  char input_path_wei_20[500];
  sprintf(input_path_wei_20,"/home/jkl596612/work/HZg/rewei/17/pho_gjet/mini_pho_gjet_test_pt20_MGG_40to80.root");
  char input_path_wei_40[500];
  sprintf(input_path_wei_40,"/home/jkl596612/work/HZg/rewei/17/pho_gjet/mini_pho_gjet_test_pt40_MGG_80toInf.root");

  char output_sig_path_wei_20to40[300];
  sprintf(output_sig_path_wei_20to40,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_sig_gjet_pt20to40_MGG_80toInf.root");
  char output_sig_path_wei_20[300];
  sprintf(output_sig_path_wei_20,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_sig_gjet_pt20_MGG_40to80.root");
  char output_sig_path_wei_40[300];
  sprintf(output_sig_path_wei_40,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_sig_gjet_pt40_MGG_80toInf.root");

  char output_bkg_path_wei_20to40[300];
  sprintf(output_bkg_path_wei_20to40,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_bkg_gjet_pt20to40_MGG_80toInf.root");
  char output_bkg_path_wei_20[300];
  sprintf(output_bkg_path_wei_20,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_bkg_gjet_pt20_MGG_40to80.root");
  char output_bkg_path_wei_40[300];
  sprintf(output_bkg_path_wei_40,"/home/jkl596612/work/HZg/MVA_5e05/17/rew_gjet/pho_bkg_gjet_pt40_MGG_80toInf.root"); 
  TString intree,outtree,intreeb;

  for(int j = 0;j<3;j++)
    {
      for(int i = 0;i<2;i++)
        {
          if(i == 0)
            {
	      intree = "sEEvent";
              outtree = "EEevent";
	      intreeb = "bEEvent";
            }
          else if(i == 1)
            {
              intree = "sBEvent";
	      outtree = "EBevent";
	      intreeb = "bBEvent";
            }
	  if(j == 0)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_sig_path_wei_20to40,
		  input_path_wei_20to40);
	    }
	else if(j == 1)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_sig_path_wei_20,
		  input_path_wei_20);
	    }
	else if(j == 2)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_sig_path_wei_40,
		  input_path_wei_40);
	    }
	}
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  for(int k = 0;k<3;k++)
    {
      for(int l = 0;l<2;l++)
	{
	  if(l == 0)
	    {
	      intree = "bEEvent";
	      outtree = "EEevent";
	      intreeb = "bEEvent";
	    }
	  else if(l == 1)
	    {
	      intree = "bBEvent";
	      outtree = "EBevent";
	      intreeb = "bBEvent";
	    }
	  if(k == 0)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_bkg_path_wei_20to40,
		  input_path_wei_20to40);
	    }
	  else if(k == 1)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_bkg_path_wei_20,
		  input_path_wei_20);
	    }
	  else if(k == 2)
	    {
	      Wei(input_path_wei_20to40,input_path_wei_20,input_path_wei_40,intree,intreeb,outtree,output_bkg_path_wei_40,
		  input_path_wei_40);
	    }	  
	}
    }

}


