#include <fstream>
#include <TH1D.h>
#include <TRandom.h>
#include <TLorentzVector.h>
#include <TCanvas.h>
#include <TLegend.h>
#include "mc_sele.h"
#include "pre_sele.h"
#include <TStyle.h>
#include <algorithm>
#include "untuplizer_07.h"

void xana(const char* input,const char* output){

  TreeReader data(input);
  gStyle->SetOptStat(1111111);
  TFile f(output,"recreate");
  TTree sBEvent("sBEvent","photon_sig");
  TTree sEEvent("sEEvent","photons_sig");
  TTree bBEvent("bBEvent","photon_bkg");
  TTree bEEvent("bEEvent","photons_bkg");
  Float_t mc_Pt;
  Float_t DeltaR;
  Float_t reco_Pt,reco_Eta,reco_Phi,Sceta,r9,rawE,sigmaIEtaIEta,Phiwidth,Etawidth,sigmaIEtaIPhi,s4,sigmaRR,Rho,phoIso,chIso,chworstIso,esEoverrawE,HoverE,diphoM,weights;
  double dR,dPt;
  Int_t nPho;
  TLorentzVector mc,reco;
  TLorentzVector reco_pho,fake_pho,di_pho;

  sBEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  sBEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  sBEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  sBEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  sBEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  sBEvent.Branch("Sceta",&Sceta,"Sceta/F");
  sBEvent.Branch("r9",&r9,"r9/F");
  sBEvent.Branch("rawE",&rawE,"rawE/F");
  sBEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  sBEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  sBEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  sBEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  sBEvent.Branch("s4",&s4,"s4/F");
  sBEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  sBEvent.Branch("Rho",&Rho,"Rho/F");
  sBEvent.Branch("phoIso",&phoIso,"phoIso/F");
  sBEvent.Branch("chIso",&chIso,"chIso/F");
  sBEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  sBEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  sBEvent.Branch("HoverE",&HoverE,"HoverE/F");

  sEEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  sEEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  sEEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  sEEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  sEEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  sEEvent.Branch("Sceta",&Sceta,"Sceta/F");
  sEEvent.Branch("r9",&r9,"r9/F");
  sEEvent.Branch("rawE",&rawE,"rawE/F");
  sEEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  sEEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  sEEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  sEEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  sEEvent.Branch("s4",&s4,"s4/F");
  sEEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  sEEvent.Branch("Rho",&Rho,"Rho/F");
  sEEvent.Branch("phoIso",&phoIso,"phoIso/F");
  sEEvent.Branch("chIso",&chIso,"chIso/F");
  sEEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  sEEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  sEEvent.Branch("HoverE",&HoverE,"HoverE/F");

  bBEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  bBEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  bBEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  bBEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  bBEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  bBEvent.Branch("Sceta",&Sceta,"Sceta/F");
  bBEvent.Branch("r9",&r9,"r9/F");
  bBEvent.Branch("rawE",&rawE,"rawE/F");
  bBEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  bBEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  bBEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  bBEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  bBEvent.Branch("s4",&s4,"s4/F");
  bBEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  bBEvent.Branch("Rho",&Rho,"Rho/F");
  bBEvent.Branch("phoIso",&phoIso,"phoIso/F");
  bBEvent.Branch("chIso",&chIso,"chIso/F");
  bBEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  bBEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  bBEvent.Branch("HoverE",&HoverE,"HoverE/F");

  bEEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  bEEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  bEEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  bEEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  bEEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  bEEvent.Branch("Sceta",&Sceta,"Sceta/F");
  bEEvent.Branch("r9",&r9,"r9/F");
  bEEvent.Branch("rawE",&rawE,"rawE/F");
  bEEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  bEEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  bEEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  bEEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  bEEvent.Branch("s4",&s4,"s4/F");
  bEEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  bEEvent.Branch("Rho",&Rho,"Rho/F");
  bEEvent.Branch("phoIso",&phoIso,"phoIso/F");
  bEEvent.Branch("chIso",&chIso,"chIso/F");
  bEEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  bEEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  bEEvent.Branch("HoverE",&HoverE,"HoverE/F");

for ( Long64_t ev = 0; ev<10000000/*data.GetEntriesFast()*/; ev++) {
    if (ev % 100000 == 0)
      fprintf(stderr, "Processing event %lli of %lli\n", ev + 1, data.GetEntriesFast());

    data.GetEntry(ev);
    Float_t* mcPt    = data.GetPtrFloat("mcPt");
    Float_t* phoEt   = data.GetPtrFloat("phoEt");
    Float_t* mcEta    = data.GetPtrFloat("mcEta");
    Float_t* phoEta   = data.GetPtrFloat("phoEta");
    Float_t* mcPhi    = data.GetPtrFloat("mcPhi");
    Float_t* phoPhi   = data.GetPtrFloat("phoPhi");
    Float_t* mcE     = data.GetPtrFloat("mcE");
    Float_t* phoE     = data.GetPtrFloat("phoE");
    Int_t   nMC  = data.GetInt("nMC");
    Int_t  nPho  = data.GetInt("nPho");
    Int_t* mcMomPID = data.GetPtrInt("mcMomPID");
    Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
    Float_t* phoSCRawE = data.GetPtrFloat("phoSCRawE");
    Float_t* phoR9Full5x5 = data.GetPtrFloat("phoR9Full5x5");
    Float_t* phoSigmaIEtaIEtaFull5x5 = data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
    Float_t* phoSCPhiWidth = data.GetPtrFloat("phoSCPhiWidth");
    Float_t* phoSCEtaWidth = data.GetPtrFloat("phoSCEtaWidth");
    Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
    Float_t* phoE2x2Full5x5 = data.GetPtrFloat("phoE2x2Full5x5");
    Float_t* phoESEffSigmaRR = data.GetPtrFloat("phoESEffSigmaRR");
    Float_t rho = data.GetFloat("rho");
    Float_t* phoPFPhoIso = data.GetPtrFloat("phoPFPhoIso");
    Float_t* phoPFChIso = data.GetPtrFloat("phoPFChIso");
    Float_t* phoPFChWorstIso = data.GetPtrFloat("phoPFChWorstIso");
    Float_t* phoESEnP1 = data.GetPtrFloat("phoESEnP1");
    Float_t* phoESEnP2 = data.GetPtrFloat("phoESEnP2");
    Float_t* phoE5x5Full5x5 = data.GetPtrFloat("phoE5x5Full5x5");
    Float_t* phoHoverE = data.GetPtrFloat("phoHoverE");
    Int_t* phohasPixelSeed = data.GetPtrInt("phohasPixelSeed");

    vector<int> acc_pho;
    vector<int> acc_reco;

    select_pho(data,acc_pho);
    pre_sele(data,acc_reco);
   
    int fake = -1;
    int signal = -1;
    double maxPt = 0;
    double MaxPt = 0;
    double Rmax = 0.1;
    double Ptmax = 0.2;

    if(acc_pho.size() < 1 || acc_reco.size() < 2)continue;   
 
    for(int i = 0; i<acc_reco.size(); i++)
	  {
	    for(int j = 0;j<acc_pho.size();j++)
	      {
		mc.SetPtEtaPhiM(mcPt[acc_pho[j]],mcEta[acc_pho[j]],mcPhi[acc_pho[j]],0.0);
		reco.SetPtEtaPhiM(phoEt[acc_reco[i]],phoEta[acc_reco[i]],phoPhi[acc_reco[i]],0.0);
		dR = mc.DeltaR(reco);
		dPt = fabs(mcPt[acc_pho[j]]-phoEt[acc_reco[i]])/mcPt[acc_pho[j]];
		if(dR<Rmax && dPt<Ptmax)
		  {
		    if(phoEt[acc_reco[i]]>MaxPt)
		      {
			MaxPt = phoEt[acc_reco[i]];
			signal = acc_reco[i];
		      } 
		    goto Loop;
		  }
		else continue;
	      }
	    if(maxPt<phoEt[acc_reco[i]])
	      {
		maxPt = phoEt[acc_reco[i]];
		fake = acc_reco[i];
	      }
	    else continue;
	  Loop:
	    continue;
	  }
     
     if(fake == -1 || signal == -1)continue;
     reco_pho.SetPtEtaPhiE(phoEt[signal],phoEta[signal],phoPhi[signal],phoE[signal]);
     fake_pho.SetPtEtaPhiE(phoEt[fake],phoEta[fake],phoPhi[fake],phoE[fake]);
     
     for(int k = 0;k<2;k++)
      {
	if(k == 0)
	  {
	    mc.SetPtEtaPhiE(mcPt[acc_pho[0]],mcEta[acc_pho[0]],mcPhi[acc_pho[0]],mcE[acc_pho[0]]);
            mc_Pt = mc.Pt();
	    r9 = phoR9Full5x5[signal];
            rawE = phoSCRawE[signal];
            sigmaIEtaIEta = phoSigmaIEtaIEtaFull5x5[signal];
            Phiwidth = phoSCPhiWidth[signal];
            Etawidth = phoSCEtaWidth[signal];
            sigmaIEtaIPhi = phoSigmaIEtaIPhiFull5x5[signal];
            s4 = phoE2x2Full5x5[signal]/phoE5x5Full5x5[signal];
            sigmaRR = phoESEffSigmaRR[signal];
            phoIso = phoPFPhoIso[signal];
            chIso = phoPFChIso[signal];
            chworstIso = phoPFChWorstIso[signal];
            esEoverrawE = (phoESEnP1[signal]+phoESEnP2[signal])/phoSCRawE[signal];
            reco_Pt = reco_pho.Pt();
            reco_Eta = reco_pho.Eta();
            reco_Phi = reco_pho.Phi();
            Sceta = phoSCEta[signal];
            Rho = rho;
            HoverE = phoHoverE[signal];

            if(fabs(Sceta)<1.4442)
              sBEvent.Fill();
            else if(fabs(Sceta)>1.566 && fabs(Sceta)<2.5)
              sEEvent.Fill();
            else continue;
	  }
	if(k == 1)
	  { 
	    mc.SetPtEtaPhiE(mcPt[acc_pho[0]],mcEta[acc_pho[0]],mcPhi[acc_pho[0]],mcE[acc_pho[0]]);
	    mc_Pt = mc.Pt();
	    r9 = phoR9Full5x5[fake];
	    rawE = phoSCRawE[fake];
	    sigmaIEtaIEta = phoSigmaIEtaIEtaFull5x5[fake];
	    Phiwidth = phoSCPhiWidth[fake];
	    Etawidth = phoSCEtaWidth[fake];
	    sigmaIEtaIPhi = phoSigmaIEtaIPhiFull5x5[fake];
	    s4 = phoE2x2Full5x5[fake]/phoE5x5Full5x5[fake];
	    sigmaRR = phoESEffSigmaRR[fake];
	    phoIso = phoPFPhoIso[fake];
	    chIso = phoPFChIso[fake];
	    chworstIso = phoPFChWorstIso[fake];
	    esEoverrawE = (phoESEnP1[fake]+phoESEnP2[fake])/phoSCRawE[fake];
	    reco_Pt = fake_pho.Pt();
	    reco_Eta = fake_pho.Eta();
	    reco_Phi = fake_pho.Phi();
	    Sceta = phoSCEta[fake];
	    Rho = rho;
	    HoverE = phoHoverE[fake];

	    if(fabs(Sceta)<1.4442)
	      bBEvent.Fill();
	    else if(fabs(Sceta)>1.566 && fabs(Sceta)<2.5)
	      bEEvent.Fill();
	    else continue;
	  }
      } 
 }
 bBEvent.Write();
 bEEvent.Write();
 sBEvent.Write();
 sEEvent.Write();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void xana_HZg(const char* input,const char* output){
  TreeReader data(input);
  gStyle->SetOptStat(1111111);
  TFile f(output,"recreate");
  TTree sBEvent("sBEvent","photon_sig");
  TTree sEEvent("sEEvent","photons_sig");
  TTree bBEvent("bBEvent","photon_bkg");
  TTree bEEvent("bEEvent","photons_bkg");
  Float_t mc_Pt;
  Float_t DeltaR;
  Float_t reco_Pt,reco_Eta,reco_Phi,Sceta,r9,rawE,sigmaIEtaIEta,Phiwidth,Etawidth,sigmaIEtaIPhi,s4,sigmaRR,Rho,phoIso,chIso,chworstIso,esEoverrawE,HoverE,diphoM;
  double dR,dPt;
  TLorentzVector mc,reco;
  TLorentzVector reco_pho,fake_pho,di_pho;

  sBEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  sBEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  sBEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  sBEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  sBEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  sBEvent.Branch("Sceta",&Sceta,"Sceta/F");
  sBEvent.Branch("r9",&r9,"r9/F");
  sBEvent.Branch("rawE",&rawE,"rawE/F");
  sBEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  sBEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  sBEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  sBEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  sBEvent.Branch("s4",&s4,"s4/F");
  sBEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  sBEvent.Branch("Rho",&Rho,"Rho/F");
  sBEvent.Branch("phoIso",&phoIso,"phoIso/F");
  sBEvent.Branch("chIso",&chIso,"chIso/F");
  sBEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  sBEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  sBEvent.Branch("HoverE",&HoverE,"HoverE/F");
  sBEvent.Branch("diphoM",&diphoM,"diphoM/F");

  sEEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  sEEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  sEEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  sEEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  sEEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  sEEvent.Branch("Sceta",&Sceta,"Sceta/F");
  sEEvent.Branch("r9",&r9,"r9/F");
  sEEvent.Branch("rawE",&rawE,"rawE/F");
  sEEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  sEEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  sEEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  sEEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  sEEvent.Branch("s4",&s4,"s4/F");
  sEEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  sEEvent.Branch("Rho",&Rho,"Rho/F");
  sEEvent.Branch("phoIso",&phoIso,"phoIso/F");
  sEEvent.Branch("chIso",&chIso,"chIso/F");
  sEEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  sEEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  sEEvent.Branch("HoverE",&HoverE,"HoverE/F");
  sEEvent.Branch("diphoM",&diphoM,"diphoM/F");

  bBEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  bBEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  bBEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  bBEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  bBEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  bBEvent.Branch("Sceta",&Sceta,"Sceta/F");
  bBEvent.Branch("r9",&r9,"r9/F");
  bBEvent.Branch("rawE",&rawE,"rawE/F");
  bBEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  bBEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  bBEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  bBEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  bBEvent.Branch("s4",&s4,"s4/F");
  bBEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  bBEvent.Branch("Rho",&Rho,"Rho/F");
  bBEvent.Branch("phoIso",&phoIso,"phoIso/F");
  bBEvent.Branch("chIso",&chIso,"chIso/F");
  bBEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  bBEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  bBEvent.Branch("HoverE",&HoverE,"HoverE/F");
  bBEvent.Branch("diphoM",&diphoM,"diphoM/F");

  bEEvent.Branch("mc_Pt",&mc_Pt,"mc_Pt/F");
  bEEvent.Branch("reco_Pt",&reco_Pt,"reco_Pt/F");
  bEEvent.Branch("DeltaR",&DeltaR,"DeltaR/F");
  bEEvent.Branch("reco_Eta",&reco_Eta,"reco_Eta/F");
  bEEvent.Branch("reco_Phi",&reco_Phi,"reco_Phi/F");
  bEEvent.Branch("Sceta",&Sceta,"Sceta/F");
  bEEvent.Branch("r9",&r9,"r9/F");
  bEEvent.Branch("rawE",&rawE,"rawE/F");
  bEEvent.Branch("sigmaIEtaIEta",&sigmaIEtaIEta,"sigmaIEtaIEta/F");
  bEEvent.Branch("Phiwidth",&Phiwidth,"Phiwidth/F");
  bEEvent.Branch("Etawidth",&Etawidth,"Etawidth/F");
  bEEvent.Branch("sigmaIEtaIPhi",&sigmaIEtaIPhi,"sigmaIEtaIPhi/F");
  bEEvent.Branch("s4",&s4,"s4/F");
  bEEvent.Branch("sigmaRR",&sigmaRR,"sigmaRR/F");
  bEEvent.Branch("Rho",&Rho,"Rho/F");
  bEEvent.Branch("phoIso",&phoIso,"phoIso/F");
  bEEvent.Branch("chIso",&chIso,"chIso/F");
  bEEvent.Branch("chworstIso",&chworstIso,"chworstIso/F");
  bEEvent.Branch("esEoverrawE",&esEoverrawE,"esEoverrawE/F");
  bEEvent.Branch("HoverE",&HoverE,"HoverE/F");
  bEEvent.Branch("diphoM",&diphoM,"diphoM/F");

  for ( Long64_t ev = 0; ev<data.GetEntriesFast(); ev++) {
    if (ev % 100000 == 0)
      fprintf(stderr, "Processing event %lli of %lli\n", ev + 1, data.GetEntriesFast());

    data.GetEntry(ev);
    Float_t* mcPt    = data.GetPtrFloat("mcPt");
    Float_t* phoEt   = data.GetPtrFloat("phoEt");
    Float_t* mcEta    = data.GetPtrFloat("mcEta");
    Float_t* phoEta   = data.GetPtrFloat("phoEta");
    Float_t* mcPhi    = data.GetPtrFloat("mcPhi");
    Float_t* phoPhi   = data.GetPtrFloat("phoPhi");
    Float_t* mcE     = data.GetPtrFloat("mcE");
    Float_t* phoE     = data.GetPtrFloat("phoE");
    Int_t   nMC  = data.GetInt("nMC");
    Int_t  nPho  = data.GetInt("nPho");
    Int_t* mcMomPID = data.GetPtrInt("mcMomPID");
    Float_t* phoSCEta = data.GetPtrFloat("phoSCEta");
    Float_t* phoSCRawE = data.GetPtrFloat("phoSCRawE");
    Float_t* phoR9Full5x5 = data.GetPtrFloat("phoR9Full5x5");
    Float_t* phoSigmaIEtaIEtaFull5x5 = data.GetPtrFloat("phoSigmaIEtaIEtaFull5x5");
    Float_t* phoSCPhiWidth = data.GetPtrFloat("phoSCPhiWidth");
    Float_t* phoSCEtaWidth = data.GetPtrFloat("phoSCEtaWidth");
    Float_t* phoSigmaIEtaIPhiFull5x5 = data.GetPtrFloat("phoSigmaIEtaIPhiFull5x5");
    Float_t* phoE2x2Full5x5 = data.GetPtrFloat("phoE2x2Full5x5");
    Float_t* phoESEffSigmaRR = data.GetPtrFloat("phoESEffSigmaRR");
    Float_t rho = data.GetFloat("rho");
    Float_t* phoPFPhoIso = data.GetPtrFloat("phoPFPhoIso");
    Float_t* phoPFChIso = data.GetPtrFloat("phoPFChIso");
    Float_t* phoPFChWorstIso = data.GetPtrFloat("phoPFChWorstIso");
    Float_t* phoESEnP1 = data.GetPtrFloat("phoESEnP1");
    Float_t* phoESEnP2 = data.GetPtrFloat("phoESEnP2");
    Float_t* phoE5x5Full5x5 = data.GetPtrFloat("phoE5x5Full5x5");
    Float_t* phoHoverE = data.GetPtrFloat("phoHoverE");
    Int_t* phohasPixelSeed = data.GetPtrInt("phohasPixelSeed");

    vector<int> acc_pho;
    vector<int> acc_reco;

    select_pho_HZg(data,acc_pho);
    pre_sele_HZg(data,acc_reco);

    int fake = -1;
    int signal = -1;
    double maxPt = 0;
    double MaxPt = 0;
    double Rmax = 0.1;
    double Ptmax = 0.2;

    if(acc_pho.size() < 1 || nPho < 2)continue;

    for(int i = 0; i<acc_reco.size(); i++)
      {
	if(phohasPixelSeed[acc_reco[i]] == 1)continue;
	for(int j = 0;j<acc_pho.size();j++)
	  {
	    mc.SetPtEtaPhiE(mcPt[acc_pho[j]],mcEta[acc_pho[j]],mcPhi[acc_pho[j]],mcE[acc_pho[j]]);
	    reco.SetPtEtaPhiE(phoEt[acc_reco[i]],phoEta[acc_reco[i]],phoPhi[acc_reco[i]],phoE[acc_reco[i]]);
	    dR = mc.DeltaR(reco);
	    dPt = fabs(mcPt[acc_pho[j]]-phoEt[acc_reco[i]])/mcPt[acc_pho[j]];
	    if(dR<Rmax && dPt<Ptmax)
	      {
		if(phoEt[acc_reco[i]]>MaxPt)
		  {
		    MaxPt = phoEt[acc_reco[i]];
		    signal = i;
		  }
		goto Loop;
	      }
	  }
	if(maxPt<phoEt[acc_reco[i]])
	  {
	    maxPt = phoEt[acc_reco[i]];
	    fake = i;
	  }
      Loop:
	continue;
      }

    if(fake == -1 || signal == -1)continue;
    reco_pho.SetPtEtaPhiE(phoEt[signal],phoEta[signal],phoPhi[signal],phoE[signal]);
    fake_pho.SetPtEtaPhiE(phoEt[fake],phoEta[fake],phoPhi[fake],phoE[fake]);
    di_pho = reco_pho+fake_pho;
    if(di_pho.M() < 55)continue;
    for(int k = 0;k<2;k++)
      {
        if(k == 0)
          {
            mc.SetPtEtaPhiE(mcPt[acc_pho[0]],mcEta[acc_pho[0]],mcPhi[acc_pho[0]],mcE[acc_pho[0]]);
            mc_Pt = mc.Pt();
            r9 = phoR9Full5x5[signal];
            rawE = phoSCRawE[signal];
            sigmaIEtaIEta = phoSigmaIEtaIEtaFull5x5[signal];
            Phiwidth = phoSCPhiWidth[signal];
            Etawidth = phoSCEtaWidth[signal];
            sigmaIEtaIPhi = phoSigmaIEtaIPhiFull5x5[signal];
            s4 = phoE2x2Full5x5[signal]/phoE5x5Full5x5[signal];
            sigmaRR = phoESEffSigmaRR[signal];
            phoIso = phoPFPhoIso[signal];
            chIso = phoPFChIso[signal];
            chworstIso = phoPFChWorstIso[signal];
            esEoverrawE = (phoESEnP1[signal]+phoESEnP2[signal])/phoSCRawE[signal];
            reco_Pt = reco_pho.Pt();
            reco_Eta = reco_pho.Eta();
            reco_Phi = reco_pho.Phi();
            Sceta = phoSCEta[signal];
            Rho = rho;
            HoverE = phoHoverE[signal];
            diphoM = di_pho.M();
            if(fabs(Sceta)<1.4442)
              sBEvent.Fill();
            else if(fabs(Sceta)>1.566 && fabs(Sceta)<2.5)
              sEEvent.Fill();
            else continue;
          }
        if(k == 1)
          {
            mc.SetPtEtaPhiE(mcPt[acc_pho[0]],mcEta[acc_pho[0]],mcPhi[acc_pho[0]],mcE[acc_pho[0]]);
            mc_Pt = mc.Pt();
            r9 = phoR9Full5x5[fake];
            rawE = phoSCRawE[fake];
            sigmaIEtaIEta = phoSigmaIEtaIEtaFull5x5[fake];
            Phiwidth = phoSCPhiWidth[fake];
            Etawidth = phoSCEtaWidth[fake];
            sigmaIEtaIPhi = phoSigmaIEtaIPhiFull5x5[fake];
            s4 = phoE2x2Full5x5[fake]/phoE5x5Full5x5[fake];
            sigmaRR = phoESEffSigmaRR[fake];
            phoIso = phoPFPhoIso[fake];
            chIso = phoPFChIso[fake];
            chworstIso = phoPFChWorstIso[fake];
            esEoverrawE = (phoESEnP1[fake]+phoESEnP2[fake])/phoSCRawE[fake];
            reco_Pt = fake_pho.Pt();
            reco_Eta = fake_pho.Eta();
            reco_Phi = fake_pho.Phi();
            Sceta = phoSCEta[fake];
            Rho = rho;
            HoverE = phoHoverE[fake];
            diphoM = di_pho.M();
            if(fabs(Sceta)<1.4442)
              bBEvent.Fill();
            else if(fabs(Sceta)>1.566 && fabs(Sceta)<2.5)
              bEEvent.Fill();
            else continue;
          }
      }
  }
  bBEvent.Write();
  bEEvent.Write();
  sBEvent.Write();
  sEEvent.Write();
}
